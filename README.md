# A Trivial Prototype of Orchestration-Based Saga Pattern using Spring StateMachine

#### (NOTE: NOT A PRODUCTION READY SYSTEM. This is merely a first-draft, VERY rudimentary prototype for demonstrating some of the possibilities of Spring Statemachine-based Sagas.) 

## Outline of Sections
The following will be discussed:

- What is demonstrated in this sample
- How to run
- High Level Architecture
- Why State Machines?
- A Few Important Notes
- Caveats and Future Enhancements

## What is demonstrated in this sample
### The state machine that was modelled here is a trivially simple Fundraiser App:
<div><img src="images/statemachinesample.png"></img></div>

> A pledge is created with a goal and a tracker for the total pledge amount. A new pledge is created with status "Requested".  Once the pledge amount exceeds the goal, the pledge is "Matched". Otherwise, donations will continue to be sent until the pledge amount is matched, or until the campaign is cancelled. At any point in time, cancelling the request will abort the campaign by refunding the pledge contributions and setting the status to "Cancelled".

<div style="height:50px; width: 50px"><img style="height:auto; width: 100%" src="images/matchpledge.png"></img></div>

> The pledge transactions are distributed transactions which span multiple databases: the Pledge database and the Donor database. Transactions are implemented using the [Saga pattern](https://microservices.io/patterns/data/saga.html).

## How to run
Currently deployed via Docker Compose; you'll need to have Docker Compose installed on your system to run.

- Clone this repository as follows:
```
    > git clone https://gitlab.com/oawofolu/ssmdemo.git
    > cd ssmdemo
    > git submodule init
    > git submodule update
```
- Start the Pledge application - this will run in the background. Wait for it to complete:
```
    > cd s1-pledgeservice
    > ./startup.sh
```
- In a separate console window, start the Donor application - this will run in the background. Wait for it to complete:
```
    > cd s1-donorservice
    > ./startup.sh
```
- In a separate (third) console window, register the Debezium connectors which will handle Change Data Capture for the Pledge and Donor databases:
```
    > ./register-connectors.sh
```
- In the third console window, save a new Pledge to the database. You can accomplish this by POSTing to the Spring Data REST endpoints exposed by the app. Here is a sample curl that will add a new Pledge to the database using the data provided by the *rest-requests/create-pledge.json* file as payload:
```
    > curl -i -H "Content-Type:application/json" --data-binary @rest-requests/create-pledge.json http://localhost:8101/pledges
```
OR you can simply go to the database and update the Pledges table directly. You should be able to point your SQL client to the database using the credentials specified under the **postgres** service in the docker-compose.yml file. (If you don't have a SQL client, you could download a free tool like DBeaver: [here](https://dbeaver.io/download/))

You should be able to observe the state transitions in the logs:
- For **_CREATING_** a new pledge, use the create-json sample request provided above. In the logs, you should observe the pledge start with the PLEDGE_REQUESTED state, continue to stay in that state as more Donor events are created, and then eventually transition to the PLEDGE_MATCHED state once the total pledged amount matches the goal. You should also be able to observe the changes in both databases by running queries against the **pledge** and **donor** tables in the **pledge**/**donor** databases respectively.
- For **_CANCELLING_** a pledge, use the cancel-json sample request (update the uuid field with the generated pledge uuid). In the logs, you should observe that the cancellation **SUCCEEDS**, and that the pledge transitions from PLEDGE_CANCEL_REQUESTED to PLEDGE_CANCEL_REQUESTED_PENDING (once the Donor service updates the **donor** DB) to PLEDGE_CANCELLED. Observe that the databases are updated accordingly.
- For handling a **_FAILED_** cancellation, reuse the same cancel-json from Step 2. The State Machine's configuration has been set up to capture cancellations on pledges that have already been cancelled as a failure condition. In the logs, you should observe that the cancellation **FAILED**, and that the pledge transitions from PLEDGE_CANCELLED to PLEDGE_CANCEL_REQUEST_PENDING. Observe that the databases are updated accordingly.

To restart, simply CTRL^C to exit out of each background runnning process, then repeat the steps above in order.

## High-Level Architecture
<div><img src="images/sample_arch.png"></img></div>

> Some of the most important layers include:
>
> **Message Broker:** Kafka was used as the underlying message broker. Aside from its reliability and fault-tolerance, its replayability is well suited to the journaling approach which is recommended for orchestration-based sagas. It can also be configured to support exactly-once delivery semantics/idempotent receiver for de-duplication where necessary.
>
> **Change Data Capture:** Debezium was used as a connector with Kafka Connect.
>
> **Saga Orchestration**: Spring Statemachine was used.
>
> **Root Aggregates**: Microservices developed using Spring Boot.
>
> **API Gateway**: Spring Cloud Gateway recommended as a future enhancement

## Why State Machines?
<div><img src="images/statemachine.png"></img></div>

> There are several patterns for modeling state (i.e. "context") in the application tier:
> 
> - Conditional branching logic: "If/then/else" statements with multiple layers of nesting, or switch statements on some discriminator, or similar. Aside from the code smell and the lack of readability of giant if/then/else statements, the state transitions are hardcoded into the business logic. There is no separation between abstraction and implementation of state transitions. This can be problematic especially in scenarios where the state transitions themselves are fairly static. (Example: Canceling an ATM card should always result in a Canceled state. Activating a new card should always result in an Activated state. Deposits should always result in an increased balance; withdrawals should always result in a decreased balance. These are static state transitions that should pretty much stay the same, regardless of the implementation details behind them. Hence, ideally, when designing the model for these state changes, it should be possible to make changes to the implementation without having to modify the state transition code itself. Conditional branching logic should suffice for systems with simpler business rules (obviously; it’s not always inappropriate to use conditional branching logic in code). But it can quickly become unwieldy at scale.
>
> - State Pattern (in object oriented languages): State is encapsulated in a separate first-class object via the State pattern. This provides better isolation from the business logic, but now the logic for driving state transitions is encapsulated in the states themselves. What if you wanted to add or remove new states? What if you wanted to reuse the states for other regions/machines? Say with our ATM example, I wanted to add a Cancel Pending state between the Active and Cancelled state. If there is currently a dependency from the Active state to the Cancelled state, I would have to refactor the code to relocate the transition logic for the Canceled state and move it from the Active state to the Cancel Pending state. Again, this reeks of needlessly tight coupling and makes the code less composable, less modular and harder to maintain, especially for systems with more complex workflows.
>
> - Loosely coupled state: Here there is no coupling between the state transitions and the business logic implementations behind them. This can be implemented using truth tables, which utilizes a more data driven approach for mapping state transitions. For a more object-oriented approach, possible options include Workflow engines, which are full fledged frameworks for managing complex enterprise workflows (Activiti, Pega, etc), as well as State Machines like Spring Statemachine, which are generally a bit more lightweight and would be suitable for use cases with simpler business rules and state transitions.


## A Few Important Notes

### Patterns Used
> Several different patterns and best practices were used, including
> - Transactional Outbox Pattern: An event-driven architecture is an obvious fit for a state machine. One option would have been to use event sourcing. However, event sourcing suffers from the Dual Writes flaw: there will likely be the need to update both the event store and the CQRS/materialized view in one transactional operation to ensure consistency (specifically "Read Your Own Writes" consistency). However, this would need to be a distributed transaction shared between the message broker (Kafka broker) and the database (PostgreSQL), which is not supported. Instead, the same transactional guarantees can be achieved by using Change Data Capture combined with an outbox table that is designed to be part of the same transaction as the source table. In this design, the outbox entity is a denormalized transform of the domain entity (ex: PledgeOutbox is the outbox entity for the Pledge domain, DonorOutbox is the outbox entity for the Donor domain). For every update to the domain entity, a corresponding change event should be reliably published to the message broker. To do this, the change events from the domain entity are stored in a holding table (the outbox table), whose transaction logs are polled by the broker for updates (via Debezium connector). Making the outbox part of the domain entity's transaction ensures that the message broker is able to consume the associated domain entity's records as part of the transaction as well. 2 outbox records will be logged: one CREATE record containing the domain entity's payload, used to publish to the message broker, and one subsequent DELETE record in order to clear out the outbox table (the DELETE does not need to be captured by the broker). For more information, see [link](https://microservices.io/patterns/data/transactional-outbox.html).
> - Spring ApplicationEvents: Used to provide loose-coupling even within the same application. For more information, see [link](https://docs.spring.io/spring-integration/reference/html/event.html).
> - Idempotent Receiver Pattern: Global unique IDs (UUIDs) were used for pledges to enable de-duplication. For more information, see [link](https://www.enterpriseintegrationpatterns.com/patterns/messaging/IdempotentReceiver.html).
> - Commutative Ops: The total pledge amount was computed on the State Machine's extended state by applying an ADD operation to every new donation. Since ADD is a _commutative_ operation, there was no need for the additional complexity of re-ordering. However, note that the ADD operation is not _idempotent_. As an enhancement, it should maintain a hash of unique IDs for de-duplication, in-memory or otherwise (aggregation was done in memory for demonstration purposes; it would be preferable to use a streaming framework like Kafka Streams to handle aggregate operations, including any necessary [fold](https://en.wikipedia.org/wiki/Fold_(higher-order_function)) operations).
> - Semantic Locking: The pledge's state includes the _PENDING suffix while the saga is executing, and reset to exclude the _PENDING prefix once it completes (or aborts).
> - Topic per domain entity: Instead of a topic per event strategy, this uses a topic per aggregate approach. This allows the State Machine to process all relevant events from the various collaborating entities in order. The Outbox entities share a denormalized schema which allows multiple entities to publish to the same topic without having to refactor to support a Schema Registry later. Also, the outbox topic associated with the Pledge root aggregate serves as a single source of truth consuming messages from the other aggregates.

### Not Used, but Should Be
- Some nifty features of Spring StateMachine, such as: 
    - Spring StateMachine reactive APIs 
    - Deferred Actions
    - Hierarchical Machines/Regions 
    - History pseudostate for compensating transactions
- Schema Registry (to preserve backward/forward data compatibility)
- StateMachineListener (more efficient event bus than the Application Context)
- StateMachineTestPlanBuilder/Unit Tests in general!
- Exactly Once Delivery Semantics
- Spring Cloud Streams (to decouple the messaging infrastructure from the application tier code)
- Persistent Statemachines
- ...etc...

### Future Enhancements
- Support for distributed state machines (see [link](https://docs.spring.io/spring-statemachine/docs/3.0.0.M2/reference/#sm-distributed))
- Enhanced demonstration of compensating actions: The Saga Orchestrator would be aware of when it needs to go into "compensating" mode. Once triggered, it would leverage Spring Statemachine's History pseudostate to replay the state machine's history in reverse (it could also leverage Kafka Streams for this purpose), and invoke the compensating action associated with each step. It would detect failed compensating actions via either a failed reply or a timeout, and respond to the failure with an appropriate policy (retry - assuming idempotency or ability for deduplication, or timeout/abort/ignore).
- ...etc...
